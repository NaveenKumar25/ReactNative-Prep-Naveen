import React, { Component } from 'react';
import { AppRegistry, Text, View, StyleSheet, } from 'react-native';

class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = {showText: true};

    // Toggle the state every second
    setInterval(() => {
      this.setState(previousState => {
        return { showText: !previousState.showText };
      });
    }, 1000);
  }

  render() {
    let display = this.state.showText ? this.props.text : ' ';
    return (
      <Text>{display}</Text>
    );
  }
}

export default class BlinkApp extends Component {
  render() {
    return (
      <View style={styles.display}>
        <Blink text='The text is blinking' />
        <Blink text='It is blinking contineusly' />
        <Blink text='At eack blink state is updated' />
        <Blink text='Look at blinking to observe the state change functionality' />
      </View>
    );
  }
}
 const styles = StyleSheet.create({
      display:{
          width: 'auto',
          height: 300,
          backgroundColor: '#2DE7DF',
          padding: 10,
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',      
      }
 })