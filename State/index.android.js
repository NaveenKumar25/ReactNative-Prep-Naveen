/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import BlinkApp from './pages/home.js'

class StateExample extends Component {
  render() {
    return (
         <BlinkApp />
    );
  }
}

AppRegistry.registerComponent('StateExample', () => StateExample);
