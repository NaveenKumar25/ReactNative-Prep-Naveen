/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
} from 'react-native';
import Camera from 'react-native-camera';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
              <Camera
                  ref={(cam) => {
                      this.camera = cam;
                  }}
                  style={styles.preview}
                  aspect={Camera.constants.Aspect.fill}>
                  <TouchableOpacity style={styles.capture} onPress={this.takePicture.bind(this)}>
                  <Image source = {require('./images/captureImg.jpeg')} style={styles.img}/>
                  </TouchableOpacity>
              </Camera>
            </View>
        );
    }

    takePicture() {
        const options = {};
        //options.location = ...
        this.camera.capture({metadata: options})
            .then((data) => console.log(data))
            .catch(err => console.error(err));
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        backgroundColor: '#000',
        flex: 0,
        borderRadius: 5,
        padding: 10,
        margin: 40,
    },
    img:{
        width: 40,
        height:40,
    }
});

