/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import FlexShow from './pages/main.js';

export default class FlexboxExample extends Component {
  render() {
    return (
          <FlexShow />
    );
  }
}

AppRegistry.registerComponent('FlexboxExample', () => FlexboxExample);
