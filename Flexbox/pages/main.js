import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class FlexShow extends Component {

  render() {
    return (
         <View>
         <View style={styles.container1}>
            <View style={styles.div1} />
            <View style={styles.div2}/>
            <View style={styles.div3}/>
         </View>
         <View style={styles.container2}>
            <View style={styles.div4} />
            <View style={styles.div5}/>
            <View style={styles.div6}/>
         </View>
         </View>
      );
   }
}
 
const styles = StyleSheet.create({
   container1: {
        flexDirection: "column",
        backgroundColor: "green",
        justifyContent: "center",
        alignItems: "center",
        height: 350,
   },
   div1: {
        width: 100,
        height: 100,
        marginBottom: 10,
        backgroundColor: "orange",
   },
   div2: {
        width: 100,
        height: 100,
        backgroundColor: "yellow",
        marginBottom: 10
   },
   div3: {
        width: 100,
        height: 100,
        backgroundColor: "blue",
   },
   container2:{
        flexDirection: "row",
        backgroundColor: "green",
        justifyContent: "center",
        alignItems: "center",
        height: 350,
   },
   div4: {
        width: 100,
        height: 100,
        backgroundColor: "orange",
   },
   div5: {
        width: 100,
        height: 100,
        backgroundColor: "yellow",
   },
   div6: {
        width: 100,
        height: 100,
        backgroundColor: "blue",
   },
})