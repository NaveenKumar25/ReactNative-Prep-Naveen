import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
} from 'react-native';

class Messages extends Component {
  render() {
    return (
      <Text style={styles.text}>Good Morning, {this.props.name}!</Text>
    );
  }
}

export default class Wishings extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Messages name='Vinay' />
        <Messages name='Ramesh' />
        <Messages name='Suresh' />
      </View>
    );
  }
}

const styles = StyleSheet.create({
     container:{
            alignItems: 'center',
            backgroundColor: '#A887B2',
            height: 700,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          },
          text:{
            fontSize: 20,
            color: '#fff',
          }
})
