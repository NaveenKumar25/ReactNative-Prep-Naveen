/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Wishings from './pages/page1.js';

class PropsExample extends Component{
    render() {
       return (
           <Wishings />
      );
   }
}

AppRegistry.registerComponent('PropsExample', () => PropsExample);
