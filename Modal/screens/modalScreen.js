import React, { Component } from 'react';

import {
   Modal,
   Text,
   TouchableHighlight,
   View,
   StyleSheet,
   Image
} 
from 'react-native'

class Navi extends Component {
   state = {
      modalVisible: false,
   }
   toggleModal(visible) {
      this.setState({ modalVisible: visible });
   }
   render() {
      return (
         <View style = {styles.container}>
            <Modal animationType = {"fade"} transparent = {true}
               visible = {this.state.modalVisible}
               onRequestClose = {() => { console.log("Modal has been closed.") } }>
               <View style = {styles.modal}>
                  <Image source = {require('../images/tigerImage.jpeg')} />
                  <Text style = {styles.text}>Modal is opened!</Text>
                  
                  <TouchableHighlight style={styles.button} onPress = {() => {
                     this.toggleModal(!this.state.modalVisible)}}>
                     
                     <Text style = {styles.text}>Close Modal</Text>
                  </TouchableHighlight>
               </View>
            </Modal>
            
            <TouchableHighlight style={styles.button} onPress = {() => {this.toggleModal(true)}}>
               <Text style = {styles.text}>Open Modal</Text>
            </TouchableHighlight>
         </View>
      )
   }
}
export default Navi;

const styles = StyleSheet.create ({
   container: {
      alignItems: 'center',
      backgroundColor: '#fff',
      padding: 100
   },
   modal: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#2DA4E7',
      padding: 100,
   },
   text: {
      color: '#fff',
   },
   button: {
      padding: 10,
      backgroundColor: '#65D4F0',
   },
})