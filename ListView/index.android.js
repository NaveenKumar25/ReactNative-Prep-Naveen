/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import List from './pages/sampleView.js';

export default class ListViewExample extends Component {
  render() {
    return (
         <List />
    );
  }
}

AppRegistry.registerComponent('ListViewExample', () => ListViewExample);
