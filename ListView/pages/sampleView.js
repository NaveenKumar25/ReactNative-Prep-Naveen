import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

export default class List extends Component {
  state = {
      names: [
         {
            id: 1,
            name: 'Robert',
         },
         {
            id: 2,
            name: 'John',
         },
         {
            id: 3,
            name: 'Rock',
         },
         {
            id: 4,
            name: 'Ronny',
         },
         {
            id: 5,
            name: 'David',
         },
         {
            id: 6,
            name: 'Kasi',
         }
      ]
   }
   alertItemName = (item) => {
      alert("You clicked " +item.name)
   }
  render() {
    return (
  
          <View> 
            {
               this.state.names.map((item, index) => (
                  <TouchableOpacity
                     key = {item.id}
                     style = {styles.container}
                     onPress = {() => this.alertItemName(item)}>
                     
                     <Text style = {styles.text}>
                        {item.name}
                     </Text>
                  </TouchableOpacity>
               ))
            }
         </View>
      )
  }
}
 
const styles = StyleSheet.create({
  container: {
      padding: 10,
      marginTop: 3,
      backgroundColor: '#d9f9b1',
      alignItems: 'center',
   },
   text: {
      color: '#4f603c'
   }
})