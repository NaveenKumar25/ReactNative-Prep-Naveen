/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import LoginFields from './pages/home.js';

export default class TextInputExample extends Component {
  render() {
    return (
         <LoginFields />
    );
  }
}

AppRegistry.registerComponent('TextInputExample', () => TextInputExample);
