/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    StyleSheet,
    ScrollView
} from 'react-native';
import {Calendar} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['fr'] = {
    monthNames: ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'],
    dayNamesShort: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
};

LocaleConfig.defaultLocale = 'fr';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.onDayPress = this.onDayPress.bind(this);
        this.onDayPressTwo =()=> {
            alert("Wow! You clicked on me ");
        };
    }

    render() {
        return (
            <ScrollView style={styles.container}>
              <Text style={styles.text}>Calendar with selectable date and arrows</Text>
              <Calendar
                  onDayPress={this.onDayPress}
                  current={'2012-02-01'}
                  style={styles.calendar}
                  markedDates={{[this.state.selected]: {selected: true}}}
              />
              <Text style={styles.text}>Calendar with marked dates and hidden arrows</Text>
              <Calendar
                  style={styles.calendar}
                  current={'2017-10-09'}
                  minDate={'2017-10-07'}
                  maxDate={'2017-10-28'}
                  onDayPress={this.onDayPressTwo }
                  firstDay={1}
                  markedDates={{
                      '2017-10-15': {selected: true, marked: true},
                      '2017-10-18': {marked: true},
                      '2017-10-26': {disabled: true}
                  }}
                  hideArrows={true}
                  theme={{
                      calendarBackground: '#B4F5E3',
                      monthTextColor: '#A350FB',
                      textSectionTitleColor: '#A350FB',
                      dayTextColor: '#A350FB',
                  }}
              />
              <Text style={styles.text}>Calendar with marked dates and spinner</Text>
              <Calendar
                  style={styles.calendar}
                  current={'2017-10-09'}
                  onDayPress={this.onDayPress}
                  minDate={'2017-10-03'}
                  displayLoadingIndicator
                  markingType={'interactive'}
                  theme={{
                      calendarBackground: '#333248',
                      textSectionTitleColor: 'white',
                      dayTextColor: 'white',
                      todayTextColor: 'white',
                      selectedDayTextColor: 'white',
                      monthTextColor: 'white',
                      selectedDayBackgroundColor: '#333248',
                      arrowColor: 'white',
                      'stylesheet.calendar.header': {
                          week: {
                              marginTop: 5,
                              flexDirection: 'row',
                              justifyContent: 'space-between'
                          }
                      }
                  }}
                  markedDates={{
                      '2017-10-05': [{textColor: '#666'}],
                      '2017-10-07': [{startingDay: true,color: 'red'}],
                      '2017-10-10': [{endingDay: true,color: 'red'}],
                      '2017-10-14': [{startingDay: true, color: 'green'}, {endingDay: true, color: 'blue'}],
                      '2017-10-22': [{startingDay: true, color: 'green'}],
                      '2017-10-23': [{ color: 'green'}],
                      '2017-10-24': [{ endingDay: true, color: 'green'}],
                  }}
                  hideArrows={false}
              />
            </ScrollView>
        );
    }

    onDayPress(day) {
        this.setState({
            selected: day.dateString
        });
    }
}

const styles = StyleSheet.create({
    calendar: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 350
    },
    text: {
        textAlign: 'center',
        borderColor: '#bbb',
        padding: 10,
        backgroundColor: '#eee'
    },
    container: {
        flex: 1,
        backgroundColor: 'gray'
    }
});
