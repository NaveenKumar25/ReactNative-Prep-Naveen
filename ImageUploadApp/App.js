/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
    Image
} from 'react-native';
import PhotoUpload from 'react-native-photo-upload'

export default class App extends Component<{}> {
  render() {
    return (
        <PhotoUpload
            onPhotoSelect={avatar => {
                if (avatar) {
                    console.log('Image base64 string: ', avatar)
                }
            }}
        >
            <Text style={styles.header}>Upload an image</Text>
            <Image
                style={styles.image}
                resizeMode='cover'
                source={{
                    uri: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
                }}
            />
        </PhotoUpload>
    );
  }
}

const styles = StyleSheet.create({
    header:{
        fontSize: 20,
        color: 'green',
        marginLeft: 4,
    },
    image:{
      paddingVertical: 30,
      width: 150,
      height: 150,
      borderRadius: 75,
  }

});

