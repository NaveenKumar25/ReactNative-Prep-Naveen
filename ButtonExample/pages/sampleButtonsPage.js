import React, { Component } from 'react';
import { Button, View, TouchableOpacity, TouchableHighlight, Text, TouchableNativeFeedback, StyleSheet  } from 'react-native';

const Home = () => {
   const handlePress = () => false
   return (
      <View>
        <Button
          onPress = {handlePress}
         title = "Normal button!"
         color = "orange"
        />
        <TouchableOpacity>
            <Text style = {styles.text}>
               TouchableOpacity Button
            </Text>
        </TouchableOpacity>
        <TouchableHighlight>
            <Text style = {styles.text}>
               TouchableHighlight Button
            </Text>
        </TouchableHighlight>
         </View>
   )
}
export default Home

const styles = StyleSheet.create ({
   text: {
      borderWidth: 1,
      padding: 10,
      paddingLeft: 125, 
      borderColor: 'black',
      backgroundColor: 'red'
   }
})
