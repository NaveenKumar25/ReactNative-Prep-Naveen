/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  ScrollView,
} from 'react-native';
import Home from './pages/images.js';

class ImagesExample extends Component {
  render() {
    return ( 
        <Home />    
    );
  }
}

AppRegistry.registerComponent('ImagesExample', () => ImagesExample);
