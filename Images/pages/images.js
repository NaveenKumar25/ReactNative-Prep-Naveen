import React, { Component } from 'react';
import { Image, View, Text, StyleSheet, ScrollView } from 'react-native';

const Home = () => (
   <ScrollView>
        <View>
            <Text style={styles.text}>Image1</Text>
            <Image style ={styles.images}  source = {require('../images/image1.jpeg')} />
        </View>
        <View>
            <Text style={styles.text}>Image2</Text>
            <Image style ={styles.images} source = {require('../images/image2.jpeg')} />
        </View>
        <View>
            <Text style={styles.text}>Image3</Text>
            <Image style ={styles.images} source = {require('../images/image3.jpeg')} />
        </View>
        <View>
            <Text style={styles.text}>Image4</Text>
            <Image style ={styles.images} source = {require('../images/image4.jpeg')} />
        </View>
        <View>
            <Text style={styles.text}>Image5</Text>
            <Image style ={styles.images} source = {require('../images/image5.jpeg')} />
        </View>
   </ScrollView>
)
export default Home;

const styles = StyleSheet.create({
    images:{
    	width: 'auto',
    	height: 150,
    	marginBottom: 10,
    },
    text:{
    	fontSize: 20,
    	color: '#0077b3',
    	marginLeft: 155,
    },
})