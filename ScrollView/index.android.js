/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import ScrollViewPage from './pages/Screen1.js';

class ScrollViewExample extends Component {
  render() {
    return (
           <ScrollViewPage />
      )
  }
}


AppRegistry.registerComponent('ScrollViewExample', () => ScrollViewExample);
