import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
} from 'react-native';

export default class ScrollViewPage extends Component {
   state = {
      names: [
         {'name': 'Naveen', 'id': 1},
         {'name': 'Hari', 'id': 2},
         {'name': 'Nikhil', 'id': 3},
         {'name': 'Sowmya', 'id': 4},
         {'name': 'Arun', 'id': 5},
         {'name': 'Ramesh', 'id': 6},
         {'name': 'Venkat', 'id': 7},
         {'name': 'Tharun', 'id': 8},
         {'name': 'Murali', 'id': 9},
         {'name': 'Gopi', 'id': 10},
      ]
   }
  render() {
    return (
           <View>
            <ScrollView>
               {
                  this.state.names.map((item, index) => (
                     <View key = {item.id} style = {styles.item}>
                        <Text style={{color: '#fff'}}>{item.name}</Text>
                     </View>
                  ))
               }
            </ScrollView>
         </View>
      )
  }
}
 
const styles = StyleSheet.create({
  item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: '#E78A2D',
   }
})