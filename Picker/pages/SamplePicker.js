import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet } from 'react-native'

class SamplePicker extends Component {
   state = {user: ''}
   ChangeSelection = (user) =>{
      this.setState({ user: user })
   }
   render() {
      return (
         <View style={styles.container}>
            <Picker selectedValue = {this.state.user} mode = {'dropdown'}  onValueChange = {this.ChangeSelection}>
                <Picker.Item label = "Select an option" value = "]



                '


                '" />
               <Picker.Item label = "Rakesh" value = "Rakesh" />
               <Picker.Item label = "Kamal" value = "Kamal" />
               <Picker.Item label = "Pushpa" value = "Pushpa" />
               <Picker.Item label = "Karishma" value = "Karishma" />
               <Picker.Item label = "Raju" value = "Raju" />
            </Picker>
            <Text style = {styles.text}>{this.state.user}</Text>
         </View>
      )
   }
}
export default SamplePicker

const styles = StyleSheet.create({
   container: {
      borderBottomColor: '#ccc',
   },
   text: {
      fontSize: 30,
      alignSelf: 'center',
      color: '#E72D87'
   }
})