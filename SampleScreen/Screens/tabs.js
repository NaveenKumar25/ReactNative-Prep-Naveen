import React, { Component } from 'react';
import { StyleSheet, View, ViewPagerAndroid } from 'react-native';
import { Container, Header, Tab, Tabs, TabHeading, Icon, Text } from 'native-base';

import Tab1 from './tabOne';
import Tab2 from './tabTwo';
import Tab3 from './tabThree';

export default class TabsExample extends Component {
    render() {
        return (
            <View style={{marginTop: 0}}>
                <Header hasTabs/>
                <Tabs>
                    <Tab heading={ <TabHeading><Icon name="home" /></TabHeading>}>
                        <Tab1 />
                    </Tab>
                    <Tab heading={ <TabHeading><Icon name="clipboard" /></TabHeading>}>
                        <Tab2 />
                    </Tab>
                    <Tab heading={ <TabHeading><Icon name="apps" /></TabHeading>}>
                        <Tab3 />
                    </Tab>
                </Tabs>
            </View>
        );
    }
}
