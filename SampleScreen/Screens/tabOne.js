import React, { Component } from 'react'
import { View,  TouchableOpacity, TextInput, StyleSheet, ViewPagerAndroid, Image, ScrollView } from 'react-native'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, List, ListItem, Separator, Thumbnail, Card, CardItem, DeckSwiper  } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

const cards = [
    {
        text: 'Card One',
        name: 'Anitha Guptha',
        image: require('./../Images/image9.jpeg'),
    },
    {
        text: 'Card Two',
        name: 'Shreya Goshal',
        image: require('./../Images/image8.jpeg'),
    },
    {
        text: 'Card Three',
        name: 'Heythin gold',
        image: require('./../Images/image7.jpeg'),
    },

];

class Tab1 extends Component {

render(){
    return (
        <View>
        <View>
            <View style={{flex: 1,height: 200}}>
                {/*<Image*/}
                {/*source = {{ uri: 'https://media.gqindia.com/wp-content/uploads/2017/09/Anushka-top-image-866x487.jpg' }}*/}
                {/*style = {{ width: 'auto', height: 200 }}*/}
                {/*/>*/}
                <DeckSwiper
                    ref={(c) => this._deckSwiper = c}
                    dataSource={cards}
                    renderEmpty={() =>
                        <View style={{ alignSelf: "center" }}>
                            <Text>Over</Text>
                        </View>}
                    renderItem={item =>
                        <Card style={{ elevation: 3 }}>

                            <CardItem cardBody>
                                <Image style={{ height: 180, flex: 1 }} source={item.image} />
                            </CardItem>
                            <CardItem>
                                <Icon name="heart" style={{ color: '#ED4A6A' }} />
                                <Text>{item.name}</Text>
                            </CardItem>
                        </Card>
                    }
                />
            </View>
            <View style={{ flexDirection: "row", flex: 1, position: "relative", top: -120, left: 0, right: 0, justifyContent: 'space-between', padding: 2 }}>
                <TouchableOpacity iconLeft onPress={() => this._deckSwiper._root.swipeLeft()}>
                    <Icon name="arrow-dropleft"  />
                </TouchableOpacity>
                <TouchableOpacity iconRight onPress={() => this._deckSwiper._root.swipeRight()}>
                    <Icon name="arrow-dropright" />
                </TouchableOpacity>
            </View>

        </View>
            <ListItem itemDivider>

            </ListItem>
        <View>
            <Grid>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="musical-notes" style={{ color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Singer</Text></View></Col>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="headset" style={{ color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Music</Text></View></Col>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="logo-playstation" style={{ color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Singer</Text></View></Col>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="reverse-camera" style={{ color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Photos</Text></View></Col>
            </Grid>
            <Grid>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="videocam" style={{ color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Video</Text></View></Col>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="american-football" style={{ color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Singer</Text></View></Col>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="female" style={{ color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Band</Text></View></Col>
                <Col style={styles.cards}><View style={styles.middleCards}><Icon name="logo-freebsd-devil" style={{color: "#DF9448"}}/><Text style={{fontWeight:'bold',fontSize: 12}}>Singer</Text></View></Col>
            </Grid>
        </View>

            <View>
                <ListItem last itemDivider>
                    <Text>Looking for an artist</Text>
                </ListItem>
                <ListItem last>
                    <Left>
                        <Icon name="search" />
                        <Text>Search Artist</Text>
                    </Left>
                    <Right>
                        <Icon name="arrow-dropright" />
                    </Right>
                </ListItem>
                <ListItem last>
                    <Left>
                        <Icon name="man" />
                        <Text>Ask Mr. Gigstart for help</Text>
                    </Left>
                    <Right>
                        <Icon name="arrow-dropright" />
                    </Right>
                </ListItem>
            </View>
            <ListItem last itemDivider>
                <Text>Popular artist in hyderabad</Text>
            </ListItem>

        <View>
            <ScrollView horizontal={true}>

                <Card>
                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image1.jpg')} style={styles.bottomCards}/>
                        <Text>Arjith Singh</Text>
                        </Body>
                    </CardItem>

                </Card>
                <Card>
                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image2.jpg')} style={styles.bottomCards}/>
                        <Text>Star2</Text>
                        </Body>
                    </CardItem>

                </Card>
                <Card>

                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image3.jpeg')} style={styles.bottomCards}/>
                        <Text>Star3</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card>

                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image4.jpg')} style={styles.bottomCards}/>
                        <Text>Star4</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card>

                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image5.jpeg')} style={styles.bottomCards}/>
                        <Text>Star5</Text>
                        </Body>
                    </CardItem>
                </Card>

                <Card>

                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image6.jpeg')} style={styles.bottomCards}/>
                        <Text>Star5</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card>

                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image7.jpeg')} style={styles.bottomCards}/>
                        <Text>Star6</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card>

                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image8.jpeg')} style={styles.bottomCards}/>
                        <Text>Star7</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card>

                    <CardItem cardBody>
                        <Body style={styles.bottomCardsText}>
                        <Image source = {require('./../Images/image9.jpeg')} style={styles.bottomCards}/>
                        <Text middle>Star8</Text>
                        </Body>
                    </CardItem>
                </Card>

            </ScrollView>
        </View>
        </View>
    )
}
}
export default Tab1;

const styles = StyleSheet.create({
    tabs:{
        marginTop: 5,
    },
    cards:{
        backgroundColor: '#fff',
        height: 100,
        borderColor: '#d6d7da',
        borderWidth: 1,
        margin:0.5,
    },
    container: {},
    // view: {
    //     margin: 10,
    //     height: 200,
    //     width: 180,
    //     //paddingHorizontal : 30
    // },
    bottomCards:{
        height: 200,
        width: 200,
        borderRadius:8,
        flex: 1,
        margin: 8
    },
    bottomCardsText:{
        alignItems:'center',
        justifyContent:'center',
    },
    middleCards:{
        alignItems:'center',
        marginTop: 24,
    }
})