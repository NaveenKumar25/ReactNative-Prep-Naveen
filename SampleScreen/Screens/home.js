// import React, { Component } from 'react';
// import {
//     Platform,
//     StyleSheet,
//     Text,
//     View,
//     TouchableHighlight,
//     ViewPagerAndroid
// } from 'react-native';
// import { StatusBar } from 'react-native';
// import TabsExample from './tabs';
//
// const instructions = Platform.select({
//     ios: 'Press Cmd+R to reload,\n' +
//     'Cmd+D or shake for dev menu',
//     android: 'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });
//
// export default class Home extends Component<{}> {
//     render() {
//         return (
//             <View>
//             <StatusBar
//                 backgroundColor="#DF4848"
//                 barStyle="light-content"
//             />
//                 {/*<TouchableHighlight style = {styles.topBar}>*/}
//                    {/*<View>*/}
//                     {/*<Text style = {styles.FirstText}>*/}
//                        {/*LOCATION*/}
//                     {/*</Text>*/}
//                     {/*<Text style = {styles.secondText}>*/}
//                        {/*Hyderabad*/}
//                     {/*</Text>*/}
//                    {/*</View>*/}
//                 {/*</TouchableHighlight>*/}
//                 <TabsExample />
//             </View>
//         );
//     }
// }
//
// const styles = StyleSheet.create({
//     topBar:{
//         backgroundColor: 'red',
//         padding: 16,
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     FirstText: {
//         color: '#fff',
//         fontSize: 11,
//         fontWeight: 'bold',
//         marginTop: 5,
//         marginLeft: 10,
//     },
//     secondText:{
//         color: '#fff',
//         fontWeight: 'bold',
//         fontSize: 16,
//     },
//
// })

import React, { Component } from 'react';
import { StyleSheet, ScrollView, Swiper, View, Image, Platform } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, List, ListItem, Separator, Thumbnail, Card, CardItem, Tab, Tabs, TabHeading  } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Tab1 from './tabOne';
import Tab2 from './tabTwo';
import Tab3 from './tabThree';

const platform = Platform.OS;

export default class Home extends Component {
    render() {
        return (
            <Container style={{backgroundColor:'#D5D5D5'}}>
                <ScrollView>
                    <Content style={{marginTop: 0}}>
                        <Header style={{backgroundColor:'red',}} hasTabs>
                            <Left />
                            <Body style={styles.alignment}>
                            <Title style={styles.titleAlignment}>Location</Title>
                            <Title style={{color: '#fff'}}>Hyderabad</Title>
                            </Body>
                            <Right />
                        </Header>
                        <Body>

                        <Tabs style={{marginTop: 0, padding:0}}>
                            <Tab heading={ <TabHeading><Icon name="home" /></TabHeading>}>
                                <Tab1 />
                            </Tab>
                            <Tab heading={ <TabHeading><Icon name="clipboard" /></TabHeading>}>
                                <Tab2 />
                            </Tab>
                            <Tab heading={ <TabHeading><Icon name="apps" /></TabHeading>}>
                                <Tab3 />
                            </Tab>
                        </Tabs>
                        </Body>
                    </Content>
                </ScrollView>
            </Container>

        );
    }
}

const styles=StyleSheet.create({
   alignment: {
       marginLeft: platform === "ios" ? 21 : 85 ,
   },
    titleAlignment:{
        marginLeft: platform === "ios" ? 0 : 25 ,
        color: '#fff',
        fontSize: 12,
    },

});
