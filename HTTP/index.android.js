/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import JsonContent from './pages/main.js';

export default class HTTPExample extends Component {
  render() {
    return (
        <JsonContent/>
    );
  }
}

AppRegistry.registerComponent('HTTPExample', () => HTTPExample);
