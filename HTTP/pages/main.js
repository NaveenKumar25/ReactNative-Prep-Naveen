import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  StyleSheet,
} from 'react-native';

export default class JsonContent extends Component {
   state = {
      data: ''
   }
   componentDidMount = ()=> {
      fetch('http://www.json-generator.com/api/json/get/bQPKLRBJhK?indent=2', {
         method: 'GET'
      })
      .then((response) => response.json())
      .then((JsonData) => {
         console.log(JsonData);
         
         this.setState({
            data: JsonData
         })
      })
      .catch((error) => {
         console.error(error);
      });
   }
  render() {
    return (
         <View style={styles.container}>
            <Text style={styles.text}>
               Name: {this.state.data.name}
            </Text>
            <Text style={styles.text}>
                Age: {this.state.data.age}
            </Text>
            <Text style={styles.text}>
                Qualification: {this.state.data.qualification}
            </Text>
            <Text style={styles.text}>
                Mobile No: {this.state.data.mobileNo}
            </Text>
            <Text style={styles.text}>
                City: {this.state.data.city}
            </Text>
            <Text style={styles.text}>
                State: {this.state.data.state}
            </Text>
         </View>
           
      )
  }
}
 
const styles = StyleSheet.create({
     container: {
      padding: 10,
      marginTop: 3,
      backgroundColor: '#d9f9b1',
   },
     text: {
      fontSize: 21,
     }
})