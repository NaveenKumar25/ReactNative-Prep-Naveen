import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class ContactUs extends Component {
    static navigationOptions = {
        title: 'ContactUs',
    };
    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={styles.container}>
                <Text>Hello I am from Contact Us page</Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})


AppRegistry.registerComponent('ContactUs', () => ContactUs);