import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class Careers extends Component {
    static navigationOptions = {
        title: 'Careers',
    };
    render() {
        const { params } = this.props.navigation.state;
        return (
            <View style={styles.container}>
                <Text>Hello I am from Careers page</Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

AppRegistry.registerComponent('Careers', () => Careers);
