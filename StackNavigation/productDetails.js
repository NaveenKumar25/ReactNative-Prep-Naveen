import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

export default class ProductsDetails extends Component {
    static navigationOptions = {
        title: 'ProductsDetails',
    };
    render() {
        const { params } = this.props.navigation.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text>Hello I am from Products details</Text>
                <Button title = 'Home' color="green"
                        onPress={() => navigate('Home')}

                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

AppRegistry.registerComponent('ProductsDetails', () => ProductsDetails);