import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View, Button
} from 'react-native';

export default class Home extends Component {
    static navigationOptions = {
        title: 'Home',
        headerBackTitle: 'Back'
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text>Hello from home</Text>
                <Button title = 'Products' color="green"
                    onPress={() => navigate('Products', {cat:'category one'})}

                />


                {/*<Button title = 'category 2' color="black"*/}
                        {/*onPress={() => navigate('Products', {cat:'category two'})}*/}

                {/*/>*/}

                <Button title = 'About us' color="green"
                        onPress={() => navigate('AboutUs')}

                />

                <Button title = 'Contact us' color="green"
                        onPress={() => navigate('ContactUs')}

                />

                <Button title = 'Careers' color="green"
                        onPress={() => navigate('Careers')}

                />
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container : {
        display :'flex',
        alignItems:'center',
        justifyContent:'center',

    }

});

AppRegistry.registerComponent('Home', () => Home);
