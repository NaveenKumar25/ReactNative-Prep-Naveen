import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Home from './home'
import Products from './products'
import AboutUs from './aboutUs'
import ContactUs from './contactUs'
import Careers from './careers'
import ProductDetails from './productDetails'

 const Navigation = StackNavigator({
     Home: { screen: Home },
     Products: { screen: Products },
     AboutUs: { screen: AboutUs },
     ContactUs: { screen: ContactUs },
     Careers: { screen: Careers },
     ProductDetails: { screen: ProductDetails}
},
     {
         mode: 'card',
        // headerMode: 'screen',
         navigationOptions: {
            gesturesEnabled: false,
         } } ,
     );


export default Navigation;