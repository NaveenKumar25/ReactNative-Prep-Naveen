import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

export default class  Products extends Component {
    static navigationOptions = {
        title: 'products',
        headerBackTitle: 'Back',
    };
    render() {
        const { params } = this.props.navigation.state;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text>Hello products</Text>
                <Button title = 'Product 1' color="green"
                        onPress={() => navigate('ProductDetails')}

                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

AppRegistry.registerComponent('Products', () => Products);
